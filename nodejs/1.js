const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'local';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);


    var c = db.collection('documents');

    //var t1 = c.findOne({tag: 't1'});

    var r = c.update({tag: 't2'}, { $inc: { count: 1}}).then(
        function (result) {
            if (result.result.n === 0) {
                c.insertOne({tag: 't2', count: 1});
            } else {
                var i = 1;
                do {
                    c.update({tag: 't2'}, { $inc: { count: 1}});
                    i++;
                } while (i < 10);
            }
            client.close(); },
        function (reason) { console.log(reason) });

    // t1 is promise
    // t1.then(function (v) {
    // if (v) {
    //     //c.update({tag: 't1'}, {$set: { count: v.count + 1}});
    //     v.count = v.count + 1;
    // } else {
    //     c.insertOne({tag: 't1', count: 1});
    // }
    // client.close()});


    console.log('done');
});

// const insertDocuments = function(db, callback) {
//     // Get the documents collection
//     const collection = db.collection('documents');
//     // Insert some documents
//     collection.insertMany([
//         {a : 1}, {a : 2}, {a : 3}
//     ], function(err, result) {
//         assert.equal(err, null);
//         assert.equal(3, result.result.n);
//         assert.equal(3, result.ops.length);
//         console.log("Inserted 3 documents into the collection");
//         callback(result);
//     });
// }

