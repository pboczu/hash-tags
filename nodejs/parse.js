const MongoClient = require('mongodb').MongoClient;

var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('test.csv')
});

MongoClient.connect('mongodb://localhost:27017', function(err, client) {

    const mongoDb = client.db('local');
    const mongoC = mongoDb.collection('tags');

    mongoC.insertOne({ job: new Date().toISOString(), stats: {}, lines: [] }, function (err,result) {
        if (err) return;

        mongoC.update({_id: result.insertedId}, {stats: { stat: 1}});

        // Doesn't work
        // https://coderwall.com/p/5okc9g/eliminating-callback-hell-in-node-js-with-synchronize
        // https://jira.mongodb.org/browse/NODE-551
        // var ar = mongoC.update({_id: result.insertedId}, {stats: { stat: 11}});
        // yield ar;


        // mongoC.update(
        //     {_id: result.insertedId},
        //     {$push: {lines: {lineNo: 1, line: 'xxxxx'}}},
        //     function (err, res) {
        //         if (err) {
        //             console.log(err);
        //         }
        //     });

        // for (var i = 1; i <= 10; i++) {
        //     mongoC.update(
        //         {_id: result.insertedId},
        //         {$push: {lines: {lineNo: i, line: i + ' X'}}},
        //         function (err, res) {
        //             if (err) {
        //                 console.log(err);
        //             }
        //         });
        // }
        //
        // client.close();

        var li = 0;
        lineReader.on('line', function (line) {
            // Stop after few lines
            if (li++ > 5) {
                process.exit(0);
            }

            mongoC.update(
                {_id: result.insertedId},
                {$push: {lines: {lineNo: li, line: line}}},
                function (err, res) {
                    if (err) {
                        console.log(err);
                    }
                });

            // First line is skipped
            // if (li > 1) {
            //     var m = line.match(/#\w+/g);
            //
            // }
        });

        //client.close();
    })
});