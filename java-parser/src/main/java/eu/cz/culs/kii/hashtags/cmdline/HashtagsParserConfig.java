package eu.cz.culs.kii.hashtags.cmdline;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.validators.PositiveInteger;
import eu.cz.culs.kii.hashtags.MatcherHelper;

@Parameters(separators = "=")
public class HashtagsParserConfig {
    @Parameter(
            names = "-m",
            description = "Name of line matcher.",
            // required = true, required only for hashtags mode
            validateWith = MatcherHelper.class,
            order = 10)
    private String matcher;

    @Parameter(
            names = "-l",
            description = "Minimum number of occurrences to a hashtag be included in output.",
            // required = true, required only for hashtags mode
            validateWith = PositiveInteger.class,
            order = 11)
    private int limit;

    public int getLimit() {
        return limit;
    }

    public String getMatcher() {
        return matcher;
    }

}
