package eu.cz.culs.kii.hashtags;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import eu.cz.culs.kii.hashtags.cmdline.CommandLineConfig;
import eu.cz.culs.kii.hashtags.cmdline.CsvConfig;
import eu.cz.culs.kii.hashtags.cmdline.TopNParserConfig;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Top 20 hashtags parser for Adéla Hamplová.
 */
public class TopNParser {
    private static class Record {
        final LocalDate date;
        final List<String> hashtags;

        Record(LocalDate date, List<String> hashtags) {
            this.date = date;
            this.hashtags = hashtags;
        }
    }

    public void main(CommandLineConfig config, CsvConfig csvConfig, TopNParserConfig topNParserConfig) throws IOException {
        System.out.println(LocalDateTime.now().toString() + " start");

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(csvConfig.getSeparator())
                //.withIgnoreQuotations(true) // MUST NOT be set
                .build();

        // Support BOM which can be added by MS Excel
        // see https://stackoverflow.com/questions/4897876/reading-utf-8-bom-marker
        BOMInputStream bOMInputStream = new BOMInputStream(Files.newInputStream(Paths.get(config.getInputFile())));
        ByteOrderMark bom = bOMInputStream.getBOM();
        Charset charsetName = bom == null ? StandardCharsets.UTF_8 : Charset.forName(bom.getCharsetName());
        BufferedReader in = new BufferedReader(new InputStreamReader(bOMInputStream, charsetName));

        CSVReader csvReader = new CSVReaderBuilder(in)
                .withCSVParser(parser)
                .withVerifyReader(false) // Documentation recommends to set to false
                .build();

        ArrayList<Record> data = new ArrayList<>();

        // Determine column indexes
        boolean headerRead = false;
        int dateColIndex;
        int textColIndex;
        if (topNParserConfig.isUseColumnNames()) {
            if (csvConfig.isNoHeader()) {
                throw new AppError("--csv-no-header cannot be combined with --use-column-names");
            }

            String[] header = csvReader.readNext();
            dateColIndex = Arrays.asList(header).indexOf("Date");
            if (dateColIndex < 0) throw new AppError("Column 'Date' not found or header is missing.");
            textColIndex = Arrays.asList(header).indexOf("Text");
            if (textColIndex < 0) throw new AppError("Column 'Text' not found or header is missing.");

            headerRead = true;
        } else {
            // Indexes derived from sample .csv provided by hamplova@pef.czu.cz
            dateColIndex = 5;
            textColIndex = 7;
        }

        // Maybe skip header
        if(!headerRead && !csvConfig.isNoHeader()) {
            csvReader.readNext();
        }

        // Phase 1 - extract and clean
        if (StringUtils.isBlank(topNParserConfig.getHashtag())) {
            throw new AppError("--hashtag must be specified");
        }
        boolean forceHash = topNParserConfig.getHashtag().startsWith("#");
        Pattern rx = Pattern.compile("([#\\w]+)");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss X uuuu", Locale.ROOT);
        String[] line;
        int processed = 0;
        int accepted = 0;
        String hashtag = topNParserConfig.getHashtag().trim().toLowerCase();
        while ((line = csvReader.readNext()) != null) {
            processed++;
            try {
                String strDate = line[dateColIndex];
                String text = line[textColIndex];

                // Sun Dec 31 23:46:06 +0000 2017
                LocalDate date = LocalDate.parse(strDate, dtf);
                Matcher m = rx.matcher(text.toLowerCase());
                ArrayList<String> hashtags = new ArrayList<>();
                while (m.find()) {
                    String word = m.group(1);
                    boolean startsWithHash = word.startsWith("#");
                    if ((forceHash && startsWithHash) || (!forceHash && !startsWithHash)) {
                        hashtags.add(word);
                    }
                }

                if (hashtags.contains(hashtag)) {
                    accepted++;
                    data.add(new Record(date, hashtags));
                }

                if ((processed % 10000) == 0) {
                    if ((processed % 100000) == 0) {
                        System.out.print(":");
                    } else {
                        System.out.print(".");
                    }
                }
            } catch (Exception ex) {
                System.out.printf("Error processing line %d: '%s'.%n", processed + 1, ex.getMessage()); // +1 because 1st row is header
            }
        }
        System.out.println();
        System.out.printf("%s Phase 1 - extract and clean completed, %d lines processed, %d lines accepted.%n", LocalDateTime.now().toString(), processed, accepted);

        // Phase 2 - write intermediate result
        String baseName = FilenameUtils.getBaseName(config.getOutputFile());
        String extension = FilenameUtils.getExtension(config.getOutputFile());
        String intermediateFileName = String.format("%s.intermediate%s%s",
                baseName,
                StringUtils.isBlank(extension) ? "" : ".",
                extension);

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(intermediateFileName), StandardCharsets.UTF_8)) {
            writer.write("Datum;Hashtagy\n");
            for (Record r : data) {
                writer.write(r.date.toString());
                writer.write(";[");
                for (int i = 0; i < r.hashtags.size(); i++) {
                    writer.write("'");
                    writer.write(r.hashtags.get(i));
                    writer.write("'");
                    if (i < r.hashtags.size() - 1) {
                        writer.write(", ");
                    }
                }
                writer.write("]\n");
            }
        }
        System.out.println(LocalDateTime.now().toString() + " Phase 2 - write intermediate result completed.");

        // Phase 3 - count top 20
        // Find frequency of hashtags.
        HashMap<String,Integer> tagToFrequency = new HashMap<>();
        for (Record r : data) {
            if (r.date == null) continue;
            if (r.date.getYear() >= topNParserConfig.getYearFrom() && r.date.getYear() <= topNParserConfig.getYearTo()) {
                for (String tag : r.hashtags) {
                    Integer count = tagToFrequency.getOrDefault(tag, 0);
                    tagToFrequency.put(tag, count + 1);
                }
            }
        }

        // Get top N hashtags
        List<String> topN = tagToFrequency.entrySet()
                .stream()
                .sorted((Map.Entry.<String,Integer>comparingByValue()).reversed())
                .limit(topNParserConfig.getTopLimit())
                .map(e -> e.getKey())
                .collect(Collectors.toList());
        System.out.println(LocalDateTime.now().toString() + String.format(" Phase 3 - count top %d completed.", topNParserConfig.getTopLimit()));

        // Phase 4 - count and write top per day
        Map<LocalDate, List<Record>> groupsByDate = data.stream().collect(Collectors.groupingBy(r -> r.date));
        Stream<Map.Entry<LocalDate, List<Record>>> sortedGroupsByDate = groupsByDate.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey());

        try (BufferedWriter w = Files.newBufferedWriter(Paths.get(config.getOutputFile()), StandardCharsets.UTF_8)) {
            w.write("Datum;");
            for (int i = 0; i < topN.size(); i++) {
                w.write(topN.get(i));
                if (i < topN.size() - 1) {
                    w.write(";");
                }
            }
            w.write("\n");

            sortedGroupsByDate.forEach(kv -> {
                Map<String, Integer> counter = createCounter(topN);
                for (Record r : kv.getValue()) {
                    for (String t : r.hashtags) {
                        if (counter.containsKey(t)) {
                            Integer c = counter.getOrDefault(t, 0);
                            counter.put(t, c + 1);
                        }
                    }
                }

                try {
                    w.write(kv.getKey().toString());
                    w.write(";");
                    for (int i = 0; i < topN.size(); i++) {
                        Integer c = counter.get(topN.get(i));
                        w.write(c.toString());
                        if (i < topN.size() - 1) {
                            w.write(";");
                        }
                    }
                    w.write("\n");
                } catch (IOException e) {
                    throw new RuntimeException((e));
                }
            });
        }
        System.out.println(LocalDateTime.now().toString() + " Phase 4 - count and write top per day completed.");

        System.out.println("Done.");
    }

    private static Map<String, Integer> createCounter(List<String> tags) {
        return tags.stream().collect(Collectors.toMap(e -> e, e -> 0));
    }
}
