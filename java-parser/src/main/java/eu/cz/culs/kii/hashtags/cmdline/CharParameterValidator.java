package eu.cz.culs.kii.hashtags.cmdline;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class CharParameterValidator implements IParameterValidator {
    @Override
    public void validate(String name, String value) throws ParameterException {
        if (value == null || value.length() != 1) {
            throw new ParameterException(String.format("Value of parameter %s must be single character.", name));
        }
    }
}
