package eu.cz.culs.kii.hashtags.cmdline;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal"})
@Parameters(separators = "=")
public class CsvConfig {
    @Parameter(names = "--csv-no-header", description = "There is no header in the csv file.", order = 20)
    private boolean noHeader = false;

    @Parameter(names = "--csv-data-column", description = "1 based index of column with data. 0 means single line", order = 21)
    private int dataColumn = 1;

    @Parameter(
            names = "--csv-separator",
            description = "Column's separator.",
            validateWith = CharParameterValidator.class,
            converter = CharParameterConverter.class,
            order = 22)
    private char separator = ',';

    public boolean isNoHeader() {
        return noHeader;
    }

    public int getDataColumn() {
        return dataColumn;
    }

    public char getSeparator() {
        return separator;
    }
}
