package eu.cz.culs.kii.hashtags;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;

public class XlsxParser extends ParserBase {

    public XlsxParser(MatcherBase matcher, Collector collector, String inputFilePath) {
        super(matcher, collector, inputFilePath);
    }

    @Override
    public void parse() throws Throwable {
        // Because source file can be quite large, XSSFWorkbook cannot be used due to poor performance
        // Use stream API instead
        // https://stackoverflow.com/questions/19192651/low-memory-writing-reading-with-apache-poi/44969009#44969009
        // https://stackoverflow.com/questions/13020658/error-while-reading-large-excel-files-xlsx-via-apache-poi

        // XSSFWorkbook workbook = new XSSFWorkbook(srcFn);
        // XSSFSheet sheet = workbook.getSheetAt(0);
        // Iterator<Row> rowIterator = sheet.rowIterator();

        OPCPackage pkg = OPCPackage.open(inputFilePath, PackageAccess.READ);
        XSSFReader r = new XSSFReader(pkg);
        ReadOnlySharedStringsTable sharedStringsTable = new ReadOnlySharedStringsTable(pkg);

        InputStream sheetStream = r.getSheetsData().next();
        InputSource sheetSource = new InputSource(sheetStream);

        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        saxFactory.setNamespaceAware(true); // This is crucial, without this settings the SheetContentsHandler is never called.
        SAXParser saxParser = saxFactory.newSAXParser();

        DefaultHandler handler = new XSSFSheetXMLHandler(r.getStylesTable(), sharedStringsTable, new XSSFSheetXMLHandler.SheetContentsHandler() {
            @Override
            public void startRow(int rowNum) {

            }

            @Override
            public void endRow(int rowNum) {

            }

            @Override
            public void cell(String cellReference, String formattedValue, XSSFComment comment) {
                processLine(formattedValue);
            }

            @Override
            public void headerFooter(String text, boolean isHeader, String tagName) {

            }
        }, false);

        saxParser.parse(sheetSource, handler);
    }
}
