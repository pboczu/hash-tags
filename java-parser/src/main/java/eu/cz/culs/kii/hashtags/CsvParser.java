package eu.cz.culs.kii.hashtags;

import eu.cz.culs.kii.hashtags.cmdline.CsvConfig;

import java.io.BufferedReader;
import java.io.FileReader;

public class CsvParser extends ParserBase {

    private final CsvConfig config;

    public CsvParser(MatcherBase matcher, Collector collector, String inputFilePath, CsvConfig config) {
        super(matcher, collector, inputFilePath);
        this.config = config;
    }

    @Override
    public void parse() throws Throwable {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFilePath))) {
            String line;
            boolean firstLine = true;
            while ((line = br.readLine()) != null) {
                if (!firstLine || config.isNoHeader()) {
                    parseLine(line);
                }
                firstLine = false;
            }
        }
    }

    private void parseLine(String line) {
        if (config.getDataColumn() == 0) {
            processLine(line);
        } else {
            String[] columns = line.split(String.valueOf(config.getSeparator()));
            processLine(columns[config.getDataColumn() - 1]);
        }
    }
}
