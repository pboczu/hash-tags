package eu.cz.culs.kii.hashtags.cmdline;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@SuppressWarnings("FieldMayBeFinal")
@Parameters(separators = "=")
public class TopNParserConfig {

    @Parameter(names = "--hashtag", description = "Hashtag user is interested in.", order = 30)
    private String hashtag;

    @Parameter(names = "--top-n", description = "How many top hashtags put to output, default 20.", order = 31)
    private int topLimit = 20;

    @Parameter(names = "--use-column-names", description = "Find columns with data by names instead fixed indexes.", order = 32)
    private boolean useColumnNames;

    @Parameter(names = "--year-from", description = "Start of year range when top hashtags are counting.", order = 33)
    private int yearFrom = Integer.MIN_VALUE;

    @Parameter(names = "--year-top", description = "End of year range when top hashtags are counting.", order = 34)
    private int yearTo = Integer.MAX_VALUE;

    public String getHashtag() {
        return hashtag;
    }

    public int getTopLimit() {
        return topLimit;
    }

    public boolean isUseColumnNames() {
        return useColumnNames;
    }

    public int getYearFrom() {
        return yearFrom;
    }

    public int getYearTo() {
        return yearTo;
    }
}
