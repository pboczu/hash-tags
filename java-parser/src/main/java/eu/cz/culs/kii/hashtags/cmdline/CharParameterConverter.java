package eu.cz.culs.kii.hashtags.cmdline;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;

public class CharParameterConverter implements IStringConverter<Character> {

    @Override
    public Character convert(String value) {
        // value is not null and of size 1 should be enforced by CharParameterValidator
        return value.charAt(0);
    }
}
