package eu.cz.culs.kii.hashtags;

import java.util.List;

public abstract class ParserBase {
    private MatcherBase matcher;
    private Collector collector;
    protected String inputFilePath;

    private int lineNo = 0;

    public ParserBase(MatcherBase matcher, Collector collector, String inputFilePath) {
        this.matcher = matcher;
        this.collector = collector;
        this.inputFilePath = inputFilePath;
    }

    protected void processLine(String line) {
        // TODO Better progress, maybe configurable.
        // Every 5000 lines print a dot to give feedback to the user.
        if (lineNo++ % 5000 == 0) System.out.print('.');

        List<String> matches = matcher.match(line);
        collector.addLine(matches);
    }

    public abstract void parse() throws Throwable;
}
