package eu.cz.culs.kii.hashtags.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import java.time.LocalDateTime;

public class MongoLab {

    public static void main(String[] argv) {
        MongoClient mc = new MongoClient();
        MongoDatabase db = mc.getDatabase("local");
        MongoCollection<Document> tags = db.getCollection("tags");


        String key = LocalDateTime.now().toString();
        Document job = new Document().append("timestamp", key);
        tags.insertOne(job);


        FindIterable<Document> found = tags.find(new Document().append("timestamp", key));
        for (Document fd : found) {
            String s = fd.toString();
        }

        Document q = new Document().append("timestamp", key);
        Document a = new Document().append("$set", Document.parse("{stats: 'stat value'}"));

        tags.updateOne(q, a);



        for (int i = 0; i < 10; i++) {
            Document p = new Document().append("$push", new Document("lines", new Document().append("lineNumber", i).append("lineText", Integer.toString(i))));
            tags.updateOne(q, p);
        }




        mc.close();
    }
}
