package eu.cz.culs.kii.hashtags.cmdline;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class CommandLineConfig {

    @Parameter(description = "<hashtags|top>", required = true, order = 1)
    private String mode;

    @Parameter(names = "-h", description = "Print this help.", help = true, order = 2)
    private boolean help;

    @Parameter(names = "-i", description = "Input file.", required = true, order = 3)
    private String inputFile;

    @Parameter(
            names = "-o",
            description = "Output file. If omitted, the file name is derived form input and placed in current directory.",
            required = true,
            order = 4)
    private String outputFile;

    public String getMode() {
        return mode;
    }

    public boolean isHelp() {
        return help;
    }

    public String getInputFile() {
        return inputFile;
    }

    public String getOutputFile() {
        return outputFile;
    }
}
