package eu.cz.culs.kii.hashtags;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.util.HashSet;
import java.util.Set;

public class MatcherHelper implements IParameterValidator {
    private static Set<Class<? extends MatcherBase>> knownMatchers = new HashSet<>();

    static {
        // I omitted dynamic resolution, e.g. with reflections.org, because I want to avoid dependencies of it.
        knownMatchers.add(HashtagsMatcher.class);
        knownMatchers.add(TagsWordsMatcher.class);
    }

    @Override
    public void validate(String name, String value) throws ParameterException {
        boolean found = false;
        for (Class<? extends MatcherBase> knownMatcher : knownMatchers) {
            found |= knownMatcher.getSimpleName().equals(value);
        }

        if (!found) {
            StringBuilder sb = new StringBuilder();
            for (Class<? extends MatcherBase> knownMatcher : knownMatchers) {
                sb
                    .append(knownMatcher.getSimpleName())
                    .append(' ');
            }
            throw new ParameterException(String.format("Wrong value of %s, valid options are: %s.", name, sb.toString()));
        }
    }

    public static MatcherBase createMatcher(String matcherName) {
        for (Class<? extends MatcherBase> knownMatcher : knownMatchers) {
            if (knownMatcher.getSimpleName().equals(matcherName)) {
                try {
                    return knownMatcher.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }

        throw new IllegalStateException(String.format("No class found for matcher %s.", matcherName));
    }
}
