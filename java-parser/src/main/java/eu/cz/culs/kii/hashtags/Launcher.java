package eu.cz.culs.kii.hashtags;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Strings;
import eu.cz.culs.kii.hashtags.cmdline.CommandLineConfig;
import eu.cz.culs.kii.hashtags.cmdline.CsvConfig;
import eu.cz.culs.kii.hashtags.cmdline.HashtagsParserConfig;
import eu.cz.culs.kii.hashtags.cmdline.TopNParserConfig;

public class Launcher {
    public static void main(String[] args) {
        String version = HashtagsParser.class.getPackage().getImplementationVersion();
        version = Strings.isStringEmpty(version) ? "[dev]" : version;
        System.out.println("Hashtags java parser v" + version);

        CommandLineConfig commonArgs = new CommandLineConfig();
        HashtagsParserConfig hashtagsParserConfig = new HashtagsParserConfig();
        CsvConfig csvConfig = new CsvConfig();
        TopNParserConfig topNParserConfig = new TopNParserConfig();

        JCommander jCommander = JCommander.newBuilder()
                .programName("parser") // Name printed in help, must comply with archiveName in task oneJar in build.gradle
                .addObject(commonArgs)
                .addObject(hashtagsParserConfig)
                .addObject(csvConfig)
                .addObject(topNParserConfig)
                .build();

        try {
            jCommander.parse(args);
        } catch (ParameterException error) {
            System.err.println(error.getMessage());
            System.err.println("Run with --help, -h for the list of available parameters.");
            System.exit(1);
        }

        if (commonArgs.isHelp()) {
            jCommander.usage();
            System.exit(0);
        }

        try {
            switch (commonArgs.getMode()) {
                case "hashtags":
                    HashtagsParser hashtagsParser = new HashtagsParser();
                    hashtagsParser.main(commonArgs, csvConfig, hashtagsParserConfig);
                    break;
                case "top":
                    TopNParser topNParser = new TopNParser();
                    topNParser.main(commonArgs, csvConfig, topNParserConfig);
                    break;
                default:
                    System.err.println("Unsupported mode");
                    System.err.println("Use -h or --help for more information about synopsis.");
                    System.exit(1);
                    break;
            }
        } catch (AppError ex) {
            System.out.println("Error, program will be terminated.");
            System.out.println(ex.getMessage());
            System.exit(1);
        }
        catch (Exception ex) {
            System.err.println(ex.toString());
            ex.printStackTrace();
        }
    }
}
