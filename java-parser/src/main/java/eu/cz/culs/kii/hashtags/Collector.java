package eu.cz.culs.kii.hashtags;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Collector {
    private ArrayList<List<String>> lines;
    private HashMap<String, Integer> tagToOccurences;

    public Collector(int estimatedSize) {
        lines = new ArrayList<>(estimatedSize);
        tagToOccurences = new HashMap<>(estimatedSize);
    }

    public void addLine(List<String> tags) {
        ArrayList<String> normalizedTags = new ArrayList<>();
        for (String tag : tags) {
            normalizedTags.add(tag.toLowerCase());
        }

        lines.add(normalizedTags);
        for (String tag : normalizedTags) {
            tagToOccurences.put(tag, tagToOccurences.getOrDefault(tag, 0) + 1);
        }
    }

    public List<String> getTags(int line, int limit) {
        ArrayList<String> tags = new ArrayList<>();
        List<String> lineTags = lines.get(line);
        for (String tag : lineTags) {
            if (tagToOccurences.getOrDefault(tag, 0) >= limit) {
                tags.add(tag);
            }
        }

        return tags;
    }

    public int size() {
        return lines.size();
    }
}
