package eu.cz.culs.kii.hashtags;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HashtagsMatcher extends RegexMatcherBase {
    private Pattern regex = Pattern.compile("#\\w+");

    @Override
    protected Matcher createMatcher(String line) {
        return regex.matcher(line);
    }
}
