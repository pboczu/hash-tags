package eu.cz.culs.kii.hashtags;

import java.util.List;

public abstract class MatcherBase {
    public abstract List<String> match(String line);
}
