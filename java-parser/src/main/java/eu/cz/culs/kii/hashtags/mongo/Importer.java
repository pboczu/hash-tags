package eu.cz.culs.kii.hashtags.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Importer {
    public static void main(String[] argv) throws IOException {

        MongoClient mc = new MongoClient();
        MongoDatabase db = mc.getDatabase("local");
        MongoCollection<Document> tags = db.getCollection("tags");

        String key = LocalDateTime.now().toString();
        Document job = new Document().append("timestamp", key);
        tags.insertOne(job);

        Document q = new Document().append("timestamp", key);
        int i = 1;
        Pattern regex = Pattern.compile("#\\w+");
        try (BufferedReader br = new BufferedReader(new FileReader("test.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                Matcher m = regex.matcher(line);

                List<String> grps = new ArrayList<>();
                while (m.find())  {
                    grps.add(m.group());
                }

                Document p = new Document()
                        .append("$push", new Document("lines",
                                new Document().append("lineNumber", i++).append("lineTags", grps)));
                tags.updateOne(q, p);
            }
        }

        mc.close();
    }
}
