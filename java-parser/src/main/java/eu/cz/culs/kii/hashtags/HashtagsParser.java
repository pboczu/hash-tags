package eu.cz.culs.kii.hashtags;

import com.beust.jcommander.Strings;
import eu.cz.culs.kii.hashtags.cmdline.CommandLineConfig;
import eu.cz.culs.kii.hashtags.cmdline.CsvConfig;
import eu.cz.culs.kii.hashtags.cmdline.HashtagsParserConfig;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.List;

public class HashtagsParser {
    public void main(CommandLineConfig config, CsvConfig csvConfig, HashtagsParserConfig parserConfig) throws IOException {

        String srcFn = config.getInputFile();

        if (!Files.exists(Paths.get(srcFn), LinkOption.NOFOLLOW_LINKS)) {
            System.out.println("Source File not found.");
            System.exit(1);
        }

        String targetFn;
        if (Strings.isStringEmpty(config.getOutputFile())) {
            String baseFn = FilenameUtils.getBaseName(srcFn);
            targetFn = String.format("%s_out.csv", baseFn); // Keep .csv as extension, or change to .txt?
        } else {
            targetFn = config.getOutputFile();
        }

        Collector collector = new Collector(100); // TODO Read from config.

        ParserBase parser = null;
        String srcExt = FilenameUtils.getExtension(srcFn).toLowerCase();
        MatcherBase matcher = MatcherHelper.createMatcher(parserConfig.getMatcher());
        switch (srcExt) {
            case "xlsx":
                parser = new XlsxParser(matcher, collector, srcFn);
                break;
            case "csv":
                parser = new CsvParser(matcher, collector, srcFn, csvConfig);
                break;
            default:
                System.out.println("Unsupported input file type.");
                System.exit(1);
                break;
        }

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(targetFn) /*, Charset.forName("UTF-8")*/)) {

            System.out.print("Parsing:");
            try {
                parser.parse();
            } catch (Throwable throwable) {
                // TODO Add better error logging.
                throwable.printStackTrace();
                System.exit(1);
            }
            System.out.println("done.");

            System.out.print("Writing:");
            for (int i = 0; i < collector.size(); i++) {
                // TODO Better progress, maybe configurable.
                // Every 5000 lines print a dot to give feedback to the user.
                if (i % 5000 == 0) System.out.print('.');

                List<String> lineTags = collector.getTags(i, parserConfig.getLimit());
                if (lineTags.isEmpty()) {
                    writer.write("** no data **");
                } else {
                    for (int ii = 0; ii < lineTags.size(); ii++) {
                        writer.write(lineTags.get(ii));
                        if (ii < lineTags.size() - 1) {
                            writer.write(' ');
                        }
                    }
                }
                writer.write(System.lineSeparator());
            }
            System.out.println("done.");

            // Solution for simple csv

        }
    }
}
