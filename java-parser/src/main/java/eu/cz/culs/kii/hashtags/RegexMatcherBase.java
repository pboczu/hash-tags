package eu.cz.culs.kii.hashtags;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public abstract class RegexMatcherBase extends MatcherBase {

    protected abstract Matcher createMatcher(String line);

    @Override
    public List<String> match(String line) {
        Matcher m = createMatcher(line);
        List<String> tags = new ArrayList<>();
        while (m.find()) {
            tags.add(m.group());
        }

        return tags;
    }
}
