package eu.cz.culs.kii.hashtags;

/**
 * Error which should cause termination of execution, e.g. invalid parameter combinaiton etc.
 */
public class AppError  extends RuntimeException {
    public AppError(String message) {
        super(message);
    }
}
